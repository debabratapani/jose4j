/*
 * Copyright 2012 Brian Campbell
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jose4j.jwx;

/**
 */
public class HeaderParameterNames
{
    public static final String ALGORITHM = "alg";

    public static final String ENCRYPTION_METHOD = "enc";

    public static final String KEY_ID = "kid";

    public static final String TYPE = "typ";

    public static final String CONTENT_TYPE = "cty";

    public static final String JWK_SET_URL = "jku";

    public static final String X509_CERTIFICATE_THUMBPRINT = "x5t";

    public static final String X509_URL = "x5u";
    
}
