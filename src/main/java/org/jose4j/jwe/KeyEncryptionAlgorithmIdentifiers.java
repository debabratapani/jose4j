/*
 * Copyright 2012 Brian Campbell
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jose4j.jwe;

/**
 */
public class KeyEncryptionAlgorithmIdentifiers
{
    public static final String RSA1_5 = "RSA1_5";
    public static final String RSA_OAEP = "RSA-OAEP";
    public static final String ECDH_ES = "ECDH-ES";
    public static final String A128KW = "A128KW";
    public static final String A256KW = "A256KW";
    public static final String A512KW = "A512KW";
    public static final String A128GCM = "A128GCM";
    public static final String A256GCM = "A256GCM";
}
